package de.tbortb.echoserver;

import de.tbortb.echoserver.models.Message;
import de.tbortb.echoserver.repositories.MessageRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Locale;
import java.util.stream.Stream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EchoServerApplicationTests {

	@Value("${prefix}")
	private String prefix;

	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	private MessageRepository messageRepo;

	@AfterEach
	void clean(){
		this.messageRepo.deleteAll();
	}

	@Test
	void echoTest() {
		String testString = "Hello World!";
		String url = "/" + testString;
		ResponseEntity<String> response = Assertions.assertDoesNotThrow(() -> restTemplate.getForEntity(url, String.class));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(prefix + testString, response.getBody());
	}

	@Test
	void shoutTest() {
		String testString = "Hello World!";
		String url = "/shout/" + testString;
		ResponseEntity<String> response = Assertions.assertDoesNotThrow(() -> restTemplate.getForEntity(url, String.class));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(prefix + testString.toUpperCase(Locale.ROOT), response.getBody());
	}

	@Test
	void saveInRepoTest(){
		String testString = "Hello World!";

		Assertions.assertDoesNotThrow(() -> restTemplate.getForEntity("/" + testString, String.class));
		Assertions.assertDoesNotThrow(() -> restTemplate.getForEntity("/shout/" + testString, String.class));

		ResponseEntity<Message[]> response = Assertions.assertDoesNotThrow(() -> restTemplate.getForEntity("/get/logs", Message[].class));

		Assertions.assertEquals(2, response.getBody().length);
		Assertions.assertTrue(Stream.of(response.getBody()).map(obj -> ((Message) obj).getText()).anyMatch(t -> t.equals(testString)));
		Assertions.assertTrue(Stream.of(response.getBody()).map(obj -> ((Message) obj).getText()).anyMatch(t -> t.equals(testString.toUpperCase(Locale.ROOT))));
	}

}
