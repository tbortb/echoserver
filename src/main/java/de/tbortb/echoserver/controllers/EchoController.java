package de.tbortb.echoserver.controllers;

import de.tbortb.echoserver.models.Message;
import de.tbortb.echoserver.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Locale;

@Controller
public class EchoController {

    @Value("${prefix}")
    private String prefix;


    @Autowired
    private MessageRepository messageRepo;

    @GetMapping("/{text}")
    public ResponseEntity<String> echo(@PathVariable String text){
        Message newMessage = new Message();
        newMessage.setText(text);
        this.messageRepo.save(newMessage);
        return ResponseEntity.ok(prefix + text);
    }

    @GetMapping("/shout/{text}")
    public ResponseEntity<String> shout(@PathVariable String text){
        Message newMessage = new Message();
        newMessage.setText(text.toUpperCase(Locale.ROOT));
        this.messageRepo.save(newMessage);
        return ResponseEntity.ok(prefix + text.toUpperCase(Locale.ROOT));
    }

    @GetMapping("/get/logs")
    public ResponseEntity<List<Message>> getLogs(){
        return ResponseEntity.ok(this.messageRepo.findAll());
    }

}
